const Admin = require('../models/Admin');
const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.register = (reqBody) => {
    let newAdmin = new Admin({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNumber : reqBody.mobileNumber,
        password : bcrypt.hashSync(reqBody.password, 10)
    });

    return Admin.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return newAdmin.save().then((user, error) => {
                if (error) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
        else {
            return false;
        }
    });
};

module.exports.login = (reqBody) => {
    return Admin.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false;
        }
        else {
            const isPasswordMatch = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordMatch) {
                return {
                    access: auth.createAccessToken(result)
                };
            }
            else {
                return false;
            }
        }
    });
};

module.exports.editProfile = (data) => {
    return Admin.findByIdAndUpdate(data.user.id, {$set : data.body}).then((result, error) => {
        if (error) {
            return error;
        }
        else {
            return true;
        }
    });
};

module.exports.addProduct = (data) => {
    let newProduct = new Product({
        adminId : data.user.id,
        title : data.body.title,
        description : data.body.description,
        brand : data.body.brand,
        category : data.body.category,
        price : data.body.price,
        stock : data.body.stock,
        thumbnail : data.body.thumbnail
    });

    return newProduct.save().then((result, error) => {
        if (error) {
            return error;
        }
        else {
            return true;
        }
    });
};

module.exports.editProduct = (data) => {
    return Product.findByIdAndUpdate(data.product.id, {$set : data.body}).then(result => {
        if (data.user.id == result.adminId) {
            return true;
        }
        else {
            return false;
        }
    });
};

module.exports.deleteProduct = (data) => {
    return Product.findByIdAndDelete(data.product.id).then(result => {
        if (data.user.id == result.adminId) {
            return true;
        }
        else {
            return false;
        }
    })
};

module.exports.archiveProduct = (data) => {
    return Product.findByIdAndUpdate(data.product.id, {isActive : false}).then(result => {
        if (data.user.id == result.adminId) {
            return true;
        }
        else {
            return false;
        }
    });
};

module.exports.activateProduct = (data) => {
    return Product.findByIdAndUpdate(data.product.id, {isActive: true}).then(result => {
        if (data.user.id == result.adminId) {
            return true;
        }
        else {
            return false;
        }
    });
}