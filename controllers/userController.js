const User = require('../models/User');
const Admin = require('../models/Admin');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email : reqBody.email}).then(rslt => {
        if (rslt.length > 0) {
            return true;
        }
        else {
            return false;
        }
    });
};

module.exports.checkDeliveryAddress = (user) => {
    return User.findById(user.id).then(user => {
        let count = user.address.length;

        if (count > 0) {
            return true;
        }
        else {
            return false;
        }
    });
}

module.exports.register = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNumber : reqBody.mobileNumber,
        password : bcrypt.hashSync(reqBody.password, 10)
    });

    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return newUser.save().then((user, error) => {
                if (error) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
        else {
            return false;
        }
    });
};

module.exports.login = (reqBody) => {
    return User.findOne({ email: reqBody.email }).then(result => {
        if (result == null) {
            return false;
        }
        else {
            const isPasswordMatch = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordMatch) {
                return {
                    access: auth.createAccessToken(result)
                };
            }
            else {
                return false;
            }
        }
    });
};

module.exports.getProfile = async (data) => {
    let result = "";
    let chkUser = await User.findById(data.id).then(rslt => {
        if (rslt !== null) {
            result = rslt;
        }
    });

    let chkAdmin = await Admin.findById(data.id).then(rslt => {
        if (rslt !== null) {
            result = rslt;
        }
    });
    
    if (result !== "") {
        return result;
    }
    else {
        return false;
    }
};

module.exports.addToCart = (data) => {
    return User.findById(data.user.id).then(user => {
        return Product.findById(data.body.productId).then((product) => {
            let item = user.cart.find(prod => prod.productId == data.body.productId);
            if (item == null) {
                user.cart.push({
                    productId : data.body.productId,
                    quantity : data.body.quantity,
                    subtotal : data.body.quantity * product.price
                });

                return user.save().then((result, error) => {
                    if (error) {
                        return error;
                    }
                    else {
                        return true;
                    }
                });
            }
            else {
                let targetIndex = user.cart.findIndex(prod => prod.productId == data.body.productId);
                let newData = {
                    productId : data.body.productId,
                    variant : data.body.variant,
                    size : data.body.size,
                    color : data.body.color,
                    quantity : item.quantity + data.body.quantity,
                    subtotal : item.subtotal + (data.body.quantity * product.price)
                }
                user.cart.splice(targetIndex, 1, newData);

                return user.save().then((result, error) => {
                    if (error) {
                        return error;
                    }
                    else {
                        return true;
                    }
                });
            }
        });
    });
};

module.exports.changePassword = (data) => {
    let newPassword = {
        password : bcrypt.hashSync(data.body.newPassword, 10)
    };

    return User.findByIdAndUpdate(data.user.id, {$set: newPassword}).then(result => {
        if (bcrypt.compareSync(data.body.oldPassword, result.password)) {
            return true;
        }
        else {
            return false;
        }
    });
};

module.exports.updateDetails = (data) => {
    return User.findByIdAndUpdate(data.user.id, {$set : data.body}).then((result, error) => {
        if (error) {
            return false;
        }
        else {
            return true;
        }
    });
};

module.exports.updateAddress = (data) => {
    return User.findById(data.user.id).then(user => {
        let count = user.address.length;
        
        if (count > 0) {
            user.address.push({
                unit : data.body.unit,
                street : data.body.street,
                subdivision : data.body.subdivision,
                district : data.body.district,
                city : data.body.city,
                postcode : data.body.postcode,
                province : data.body.province
            });

            return user.save().then((result, error) => {
                if (error) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
        else {
            user.address.push({
                unit : data.body.unit,
                street : data.body.street,
                subdivision : data.body.subdivision,
                district : data.body.district,
                city : data.body.city,
                postcode : data.body.postcode,
                province : data.body.province,
                isPrimary : true
            });

            return user.save().then((result, error) => {
                if (error) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
    });
}

module.exports.deleteAddress = (data) => {
    return User.findById(data.user.id).then(user => {
        let targetAddr = user.address.find(addr => addr._id === data.body.addrId);

        if (targetAddr !== "") {
            let targetIndex = user.address.findIndex(ind => ind._id === data.body.addrId);

            user.address.splice(targetIndex, 1);

            return user.save().then((result, error) => {
                if (error) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
        else {
            return false;
        }
    });
};

module.exports.viewAddress = (user) => {
    return User.findById(user.id).then(user => {
        let addrList = user.address;

        if (addrList.length > 0) {
            return addrList;
        }
        else {
            return false;
        }
    })
};

module.exports.viewCart = (user) => {
    return User.findById(user.id).then((result, error) => {
        if (error) {
            return error;
        }
        else {
            return result.cart;
        }
    });
};

module.exports.order = async (data) => {
    let placeOrder = await User.findById(data.user.id).then(user => {
        let updatedProduct = {
            $inc: {stock: -data.body.quantity}
        };

        return Product.findByIdAndUpdate(data.body.productId, updatedProduct).then(product => {
            if (product.stock > data.body.quantity) {
                let address = user.address.find(adr => adr.isPrimary === true);
                let newOrder = new Order({
                    userId : data.user.id,
                    productId : data.body.productId,
                    quantity : data.body.quantity,
                    total : data.body.total
                });

                return newOrder.save().then((result, error) => {
                    if (error) {
                        return false;
                    }
                    else {
                        return true;
                    }
                });
            }
            else {
                return false;
            }
        });
    });

    let removeFromCart = await User.findById(data.user.id).then(user => {
        let isExist = user.cart.find(prod => prod.productId === data.body.productId);

        if (isExist) {
            let targetIndex = user.cart.findIndex(prod => prod.productId == data.body.productId);
            user.cart.splice(targetIndex, 1);

            return user.save().then((result, error) => {
                if (error) {
                    return false;
                }
                else {
                    return true;
                }
            });
        }
        return true;
    });

    if (placeOrder && removeFromCart) {
        return true;
    }
    else {
        return false;
    }
};

module.exports.getOrders = (data) => {
    return Order.find({userId: data.id}).then(order => {
        if (order !== null) {
            return order;
        }
        else {
            return false;
        }
    });
};