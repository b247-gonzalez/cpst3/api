const Product = require('../models/Product');
const Admin = require('../models/Admin');
const User = require('../models/User');

module.exports.viewAll = () => {
    return Product.find({}).then(result => {
        if (result == null) {
            return false;
        }
        else {
            return result;
        }
    });
};

module.exports.viewActive = () => {
    return Product.find({isActive: true}).then(result => {
        if (result == null) {
            return false;
        }
        else {
            return result;
        }
    })
};

module.exports.viewProduct = (reqParams) => {
    return Product.findById(reqParams.id).then(result => {
        if (result == null) {
            return false;
        }
        else {
            return result;
        }
    });
};