const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const port = process.env.SERVER_PORT || 4004;

const userRoutes = require('./routes/userRoutes');
const adminRoutes = require('./routes/adminRoutes');
const productRoutes = require('./routes/productRoutes');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});


mongoose.connect('mongodb+srv://lng-athens:BVgvPPJIEu6qjm8V@zuitt-bootcamp.oquvder.mongodb.net/capstone3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.on("open", () => { console.log("Connected to MongoDB Atlas") });

app.use("/user", userRoutes);
app.use("/admin", adminRoutes);
app.use("/product", productRoutes);

app.listen(port, () => console.log(`Server is now running at port ${port}`));