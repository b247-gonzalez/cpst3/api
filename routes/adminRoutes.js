const express = require('express');
const router = express.Router();
const adminController = require('../controllers/adminController');
const auth = require('../auth');

router.post("/register", (req, res) => {
    adminController.register(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
    adminController.login(req.body).then(resultFromController => res.send(resultFromController));
});

router.patch("/edit", auth.isAdminVerify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        body : req.body
    };

    adminController.editProfile(data).then(resultFromController => res.send(resultFromController));
});

router.post("/addProduct", auth.isAdminVerify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        body : req.body
    };

    adminController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/product/edit/:id", auth.isAdminVerify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        product: req.params,
        body : req.body
    };

    adminController.editProduct(data).then(resultFromController => res.send(resultFromController));
});

router.delete("/product/delete/:id", auth.isAdminVerify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        product : req.params
    };

    adminController.deleteProduct(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/product/archive/:id", auth.isAdminVerify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        product : req.params
    };

    adminController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/product/activate/:id", auth.isAdminVerify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        product : req.params
    };

    adminController.activateProduct(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;