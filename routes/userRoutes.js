const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/checkDeliveryAddress", auth.verify, (req, res) => {
    let user = auth.decode(req.headers.authorization);

    userController.checkDeliveryAddress(user).then(resultFromController => res.send(resultFromController));
});

router.post("/register", (req, res) => {
    userController.register(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/login", (req, res) => {
    userController.login(req.body).then(resultFromController => res.send(resultFromController));
});

router.get("/details", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
});

router.patch("/changePassword", auth.verify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        body : req.body
    };

    userController.changePassword(data).then(resultFromController => res.send(resultFromController));
});

router.patch("/updateDetails", auth.verify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        body : req.body
    };

    userController.updateDetails(data).then(resultFromController => res.send(resultFromController));
});

router.post("/updateAddress", auth.verify, (req, res) => {
    let data = {
        user: auth.decode(req.headers.authorization),
        body: req.body
    };

    userController.updateAddress(data).then(resultFromController => res.send(resultFromController));
});

router.delete("/deleteAddress", auth.verify, (req, res) => {
    let data = {
        user: auth.decode(req.headers.authorization),
        body : req.body
    }

    userController.deleteAddress(data).then(resultFromController => res.send(resultFromController));
});

router.get("/addressList", auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    userController.viewAddress(userData).then(resultFromController => res.send(resultFromController));
});

router.post("/addToCart", auth.verify, (req, res) => {
    let data = {
        user : auth.decode(req.headers.authorization),
        body : req.body
    };

    userController.addToCart(data).then(resultFromController => res.send(resultFromController));
});

router.get("/cart", auth.verify, (req, res) => {
    let user = auth.decode(req.headers.authorization);

    userController.viewCart(user).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout", auth.verify, (req, res) => {
    let data = {
        user: auth.decode(req.headers.authorization),
        body: req.body
    };

    userController.order(data).then(resultFromController => res.send(resultFromController));
});

router.get('/getOrders', auth.verify, (req, res) => {
    let userData = auth.decode(req.headers.authorization);

    userController.getOrders(userData).then(resultFromController => res.send(resultFromController));
});

module.exports = router;