const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

router.get("/view/all", (req, res) => {
    productController.viewAll().then(resultFromController => res.send(resultFromController));
});

router.get("/view/active", (req, res) => {
    productController.viewActive().then(resultFromController => res.send(resultFromController));
});

router.get("/view/:id", (req, res) => {
    productController.viewProduct(req.params).then(resultFromController => res.send(resultFromController));
});

module.exports = router;