const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    adminId : {
        type : String,
        required : [true, "Seller Id required"]
    },
    title : {
        type : String,
        required : [true, "Product name required"]
    },
    description : {
        type : String,
        required : [true, "Product description required"]
    },
    brand : {
        type : String,
        required : [true, "Product brand required"]
    },
    category : {
        type : String,
        required : [true, "Category required"]
    },
    price : {
        type : Number,
        required : [true, "Product price required"]
    },
    discount: {
        type : Number,
        default : 0
    },
    stock : {
        type : Number,
        default : 1
    },
    thumbnail: {
        type : String,
        required : [true, "Thumbnail url required"]
    },
    images : [
        {
            url : {
                type : String,
                required : [true, "Image url required"]
            }
        }
    ],
    isActive : {
        type : Boolean,
        default : true
    },
    createdOn : {
        type : Date,
        default : new Date()
    },

});

module.exports = mongoose.model("Product", productSchema);