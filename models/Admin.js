const mongoose = require('mongoose');

const adminSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name required"]
    },
    email : {
        type : String,
        required : [true, "Email required"]
    },
    mobileNumber : {
        type : String,
        required : [true, "Mobile number required"]
    },
    password : {
        type : String,
        required : [true, "Password required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    createdOn : {
        type : Date,
        default : new Date()
    }
});

module.exports = mongoose.model("Admin", adminSchema);