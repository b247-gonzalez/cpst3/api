const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name required"]
    },
    email : {
        type : String,
        required : [true, "Email required"]
    },
    mobileNumber : {
        type : String,
        required : [true, "Mobile number required"]
    },
    password : {
        type : String,
        required : [true, "Password required"]
    },
    cart : [
        {
            productId : {
                type : String,
                required : [true, "Product Id required"]
            },
            quantity : {
                type : Number,
                required : [true, "Quantity required"]
            },
            subtotal : {
                type : Number,
                required : [true, "Subtotal required"]
            }
        }
    ],
    address : [
        {
            unit : {
                type : String,
                required : [true, "Unit required"]
            },
            street : {
                type: String,
                required : [true, "Street required"]
            },
            subdivision : {
                type : String
            },
            district : {
                type : String,
                required : [true, "District required"]
            },
            city : {
                type : String,
                required : [true, "City required"]
            },
            postcode : {
                type : Number,
                required : [true, "Postal code required"]
            },
            province : {
                type : String,
                required : [true, "Province required"]
            },
            isPrimary : {
                type : Boolean,
                default : false
            }
        }
    ],
    isAdmin : {
        type : Boolean,
        default : false
    },
    createdOn : {
        type : Date,
        default : new Date()
    }
});

module.exports = mongoose.model("User", userSchema);