const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    userId : {
        type: String,
        required : [true, "User Id required"]
    },
    productId : {
        type : String,
        required : [true, "Product Id required"]
    },
    quantity : {
        type : Number,
        required : [true, "Quantity required"]
    },
    total : {
        type : Number,
        required : [true, "Total price required"]
    },
    isPaid : {
        type : Boolean,
        default : false
    },
    status : {
        type : String,
        default : "pending"
    }
});

module.exports = mongoose.model("Order", orderSchema);